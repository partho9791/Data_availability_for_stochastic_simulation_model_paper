set terminal postscript eps enhanced color

set key font "Century Schoolbook L,22"

#set key outside
#set key at 9, 99

set key Left

unset key

set mxtics 2

#set logscale y

set key samplen 2.5

set ytics 20

set yrange [20:109]

set xrange [0:109]

set border 1+2 back linewidth 2

set xtics font ",22" nomirror
set ytics font ",22" nomirror


set arrow from graph 0,0.95 to graph 0,1.01 size screen 0.025,15,60 filled
set arrow from graph 0.95,0 to graph 1.01,0 size screen 0.025,15,60 filled


set xlabel "r_{/*1 enzyme} (nm)" font ",22"


set ylabel "Glucan released at 26 hours \n \n {/Symbol m}g Glc/mg cell wall prep (AIR) " font ",22" offset -0.85,-0


set termoption dashed

#unset key


set output 'Output/plots/Sacc_Enz_size_trend_color.eps'


f(x) = a*x*x + b*x + c

fit f(x) "Output/size_v_yield.txt" via a,b,c

plot "Output/size_v_yield.txt" w p pt 7 ps 3 lc rgb "blue" notitle, f(x) w l lw 6 dt 5 lc rgb "blue" title "ax^2 + bx + c"

#plot "Output/mean_sacc/mean_saccharification_size_5.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#AAAAFF" title "r_{/*0.5 enzyme} = 5", "Output/mean_sacc/mean_saccharification_size_10.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#7777FF" title "r_{/*0.5 enzyme} = 10", "Output/mean_sacc/mean_saccharification_size_25.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#3333FF" title "r_{/*0.5 enzyme} = 25", "Output/mean_sacc/mean_saccharification_size_50.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#0000FF" title "r_{/*0.5 enzyme} = 50", "Output/mean_sacc/mean_saccharification_size_100.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#000077" title "r_{/*0.5 enzyme} = 100"
