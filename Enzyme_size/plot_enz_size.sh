set terminal postscript eps enhanced color

set key font "Century Schoolbook L,22"

#set key outside
set key at 9, 99

set key Left


set mxtics 2

#set logscale y

set key samplen 2.5

set ytics 20

set yrange [0:109]

set xrange [0:27]

set border 1+2 back

set xtics font ",22" nomirror
set ytics font ",22" nomirror


set arrow from graph 0,0.95 to graph 0,1.01 filled
set arrow from graph 0.95,0 to graph 1.01,0 filled


set xlabel "Time (hours)" font ",22"


set ylabel "{/Symbol m}g Glc/mg cell wall prep (AIR) " font ",22"


set termoption dashed



set output 'Output/plots/Sacc_Enz_size_color.eps'



plot "Output/mean_sacc/mean_saccharification_size_5.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#AAAAFF" title "r_{/*0.5 enzyme} = 5", "Output/mean_sacc/mean_saccharification_size_10.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#7777FF" title "r_{/*0.5 enzyme} = 10", "Output/mean_sacc/mean_saccharification_size_25.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#3333FF" title "r_{/*0.5 enzyme} = 25", "Output/mean_sacc/mean_saccharification_size_50.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#0000FF" title "r_{/*0.5 enzyme} = 50", "Output/mean_sacc/mean_saccharification_size_100.txt" using 1:($2*0.299163/8.01232) w l dt 2 lw 6 lc rgb "#000077" title "r_{/*0.5 enzyme} = 100"
