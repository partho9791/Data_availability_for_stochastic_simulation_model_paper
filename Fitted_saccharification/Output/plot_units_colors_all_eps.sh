set terminal postscript eps enhanced color
set termoption enhanced

set key font " ,22"

#set key outside
#set key above

set key bottom right

set key Left

set mxtics 2
set mytics 2

#set logscale y

set key samplen 0.5



set xrange [0:27]

set border 1+2 back linewidth 3


set xlabel "Time (hours)" font ",22"


set ylabel "{/Symbol m}g Glc/mg cell wall prep (AIR)" font ",22"

set ytics 20

set xtics font ",22" nomirror
set ytics font ",22" nomirror


set termoption dashed

set arrow from graph 0,0.95 to graph 0,1.01 size screen 0.025,30,60 filled
set arrow from graph 0.95,0 to graph 1.01,0 size screen 0.025,30,60 filled


set output 'PLOTS/Best_fits_all_colors.eps'

set yrange [0:129]

plot 'mean_sacc/mean_saccharification_a619.txt' using 1:($2*0.299163/8.01232) w l dt 2 lw 7 lc rgb "#FF0000"  notitle, 'expe_data/expe_saccharification_a619_glc_zz.txt' using 1:2:3 w yerrorbars ps 2.5 pt 6 lw 2 lc rgb "#FF0000" title '{/Times-Roman A619}', 'mean_sacc/mean_saccharification_cal1.txt' using 1:($2*0.299163/6.62359) w l dt 2 lw 7 lc rgb "#FF00FF"  notitle, 'expe_data/expe_saccharification_cal1_glc_zz.txt' using 1:2:3 w yerrorbars ps 2.5 pt 10 lw 2 lc rgb "#FF00FF" title '{/Times-Italic cal1}', 'mean_sacc/mean_saccharification_cal2.txt' using 1:($2*0.299163/8.97002) w l dt 2 lw 7 lc rgb "#00FF00"  notitle, 'expe_data/expe_saccharification_cal2_glc_zz.txt' using 1:2:3 w yerrorbars ps 2.5 pt 8 lw 2 lc rgb "#00FF00" title '{/Times-Italic cal2}', 'mean_sacc/mean_saccharification_cal3.txt' using 1:($2*0.299163/8.75783) w l dt 2 lw 7 lc rgb "#0000FF"  notitle, 'expe_data/expe_saccharification_cal3_glc_zz.txt' using 1:2:3 w yerrorbars ps 2.5 pt 4 lw 2 lc rgb "#0000FF" title '{/Times-Italic cal3}', 'mean_sacc/mean_saccharification_bm1.txt' using 1:($2*0.299163/6.68442) w l dt 2 lw 7 lc rgb "#7F00FF"  notitle, 'expe_data/expe_saccharification_bm1_glc_zz.txt' using 1:2:3 w yerrorbars ps 2.5 pt 12 lw 2 lc rgb "#7F00FF" title '{/Times-Italic bm1}', 'mean_sacc/mean_saccharification_bm3.txt' using 1:($2*0.299163/7.29108) w l dt 2 lw 7 lc rgb "#007FFF"  notitle, 'expe_data/expe_saccharification_bm3_glc_zz.txt' using 1:2:3 w yerrorbars ps 2.5 pt 14 lw 2 lc rgb "#007FFF" title '{/Times-Italic bm3}'

