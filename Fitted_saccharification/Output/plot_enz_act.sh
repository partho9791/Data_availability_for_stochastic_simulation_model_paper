set terminal postscript eps enhanced color

set key font ",22"

#set key outside
#set key above

set key top right

set key Left

set mxtics 2
set mytics 2

set ytics 0.2

set xtics font ",22" nomirror
set ytics font ",22" nomirror

set border 1+2 back linewidth 2

set arrow from graph 0,0.95 to graph 0,1.01 size screen 0.025,15,60 filled
set arrow from graph 0.95,0 to graph 1.01,0 size screen 0.025,15,60 filled

#set logscale y

set key samplen 2.5

set yrange [0:1.05]
set xrange [0:27]


set xlabel "Time (hours)" font ",22"


set ylabel "Simulated enzyme activity " font ",22"


set termoption dashed



set output 'PLOTS/Enz_activity_a619_color.eps'

plot "mean_activity/mean_activity_a619.txt" u 1:2 w l lw 9 dt 9 lc rgb "#0000FF" title "EG", "mean_activity/mean_activity_a619.txt" u 1:3 w l lw 9 dt 9 lc rgb "#00FF00" title "CBH", "mean_activity/mean_activity_a619.txt" u 1:4 w l lw 9 dt 9 lc rgb "#FF00FF" title "BGL", "mean_activity/mean_activity_a619.txt" u 1:5 w l lw 9 dt 9 lc rgb "#FF0000" title "HC"



set output 'PLOTS/Enz_activity_cal1_color.eps'

plot "mean_activity/mean_activity_cal1.txt" u 1:2 w l lw 9 dt 9 lc rgb "#0000FF" title "EG", "mean_activity/mean_activity_cal1.txt" u 1:3 w l lw 9 dt 9 lc rgb "#00FF00" title "CBH", "mean_activity/mean_activity_cal1.txt" u 1:4 w l lw 9 dt 9 lc rgb "#FF00FF" title "BGL", "mean_activity/mean_activity_cal1.txt" u 1:5 w l lw 9 dt 9 lc rgb "#FF0000" title "HC"