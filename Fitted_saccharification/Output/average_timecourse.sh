#$1: number of runs

mkdir mean_sacc


name="cal1"
python3 calc_mean_interpolate.py $1 saccharification/saccharification_${name}_  mean_sacc/mean_saccharification_${name}.txt  expe_data/expe_saccharification_${name}_ &
name="cal2"
python3 calc_mean_interpolate.py $1 saccharification/saccharification_${name}_  mean_sacc/mean_saccharification_${name}.txt  expe_data/expe_saccharification_${name}_ &
name="cal3"
python3 calc_mean_interpolate.py $1 saccharification/saccharification_${name}_  mean_sacc/mean_saccharification_${name}.txt  expe_data/expe_saccharification_${name}_ &
name="a619"
python3 calc_mean_interpolate.py $1 saccharification/saccharification_${name}_  mean_sacc/mean_saccharification_${name}.txt  expe_data/expe_saccharification_${name}_ &
name="bm1"
python3 calc_mean_interpolate.py $1 saccharification/saccharification_${name}_  mean_sacc/mean_saccharification_${name}.txt  expe_data/expe_saccharification_${name}_ &
name="bm3"
python3 calc_mean_interpolate.py $1 saccharification/saccharification_${name}_  mean_sacc/mean_saccharification_${name}.txt  expe_data/expe_saccharification_${name}_ &

