set terminal postscript eps enhanced color

#set terminal eps size 1280, 720

set key font ",22"

#set key outside
#set key bottom right
set key inside bottom center horizontal

set key spacing 1.5


set xlabel "Time (hours)" font ",22"
set ylabel "{/Symbol m}g Glc/mg cell wall prep (AIR)" font ",22"

set xtics font ",22" nomirror
set ytics font ",22" nomirror
set ytics 20


#set ytics rotate by 60

set mxtics 2

unset key


set termoption dashed
set termopt enhanced

set border 1+2 back linewidth 2


set arrow from graph 0,0.95 to graph 0,1.01 size screen 0.025,15,60 filled
set arrow from graph 0.95,0 to graph 1.01,0 size screen 0.025,15,60 filled

set xrange [0:27]
set yrange [0:110]

set output 'Output/plots/sacc_time_courses_both_color.eps'
#set title "{/Symbol=18 \362@_{/=9.6 0}^{/=12 x}}" font ", 42"

set label "{/Symbol= 30w}@^{/*0.7 cbs,glc}_{/*0.7 CBH,EG,BGL} = 0.0" font ",22" at 5,100

set label "{/Symbol= 30w}@^{/*0.7 cbs,glc}_{/*0.7 CBH,EG,BGL} = 1.0" font ",22" at 20,40


p "Output/mean_sacc/mean_saccharification_both_p0.txt" u 1:($2*0.299163/8.01232) w l lw 9 dt 2 lc rgb "#AAAAFF" title "{/Symbol= 30w}@^{/*0.7 cbs,glc}_{/*0.7 CBH,EG,BGL} = 0.0", "Output/mean_sacc/mean_saccharification_both_p2.txt" u 1:($2*0.299163/8.01232) w l lw 9 dt 2 lc rgb "#7777FF" title "{/Symbol= 30w}@^{/*0.7 cbs,glc}_{/*0.7 CBH,EG,BGL} = 0.2", "Output/mean_sacc/mean_saccharification_both_p4.txt" u 1:($2*0.299163/8.01232) w l lw 9 dt 2 lc rgb "#3333FF" title "{/Symbol= 30w}@^{/*0.7 cbs,glc}_{/*0.7 CBH,EG,BGL} = 0.4", "Output/mean_sacc/mean_saccharification_both_p6.txt" u 1:($2*0.299163/8.01232) w l lw 9 dt 2 lc rgb "#0000FF" title "{/Symbol= 30w}@^{/*0.7 cbs,glc}_{/*0.7 CBH,EG,BGL} = 0.6", "Output/mean_sacc/mean_saccharification_both_p8.txt" u 1:($2*0.299163/8.01232) w l lw 9 dt 2 lc rgb "#0000AA" title "{/Symbol= 30w}@^{/*0.7 cbs,glc}_{/*0.7 CBH,EG,BGL} = 0.8", "Output/mean_sacc/mean_saccharification_both_p10.txt" u 1:($2*0.299163/8.01232) w l lw 9 dt 2 lc rgb "#000077" title "{/Symbol= 30w}@^{/*0.7 cbs,glc}_{/*0.7 CBH,EG,BGL} = 1.0"
