set terminal postscript eps enhanced color

#set terminal eps size 1280, 720

set key font ",22"

#set key outside
#set key bottom right
set key inside bottom center horizontal

set key spacing 1.5
set key samplen 0.8



set ylabel "{/Symbol m}g Glc/mg cell wall prep (AIR)" font ",22"

set xtics font ",22" nomirror
set ytics font ",22" nomirror

set ytics 20
#set ytics rotate by 60
set yrange [0:110]
set xrange [0:1.07]


set mxtics 2


set termoption dashed
set termopt enhanced

set border 1+2 back linewidth 2


set arrow from graph 0,0.95 to graph 0,1.01 size screen 0.025,15,60 filled
set arrow from graph 0.95,0 to graph 1.01,0 size screen 0.025,15,60 filled



set output 'Output/plots/sacc_inhib_both_color.eps'
set xlabel '{/Symbol= 30w}@^{/*0.7 cbs,glc}_{/*0.7 EG,CBH,BGL}' font ",22"

p "Output/mean_sacc_time/zz_sacc_v_inhib_BOTH_T5.txt" u 1:($3*0.299163/8.01232) w p pt 4 ps 2.5 lw 4 dt 2 lc rgb "#9999FF" title "T=5h", "Output/mean_sacc_time/zz_sacc_v_inhib_BOTH_T10.txt" u 1:($3*0.299163/8.01232) w p pt 5 ps 2.5 lw 4 dt 3 lc rgb "#5555FF" title "T=10h", "Output/mean_sacc_time/zz_sacc_v_inhib_BOTH_T25.txt" u 1:($3*0.299163/8.01232) w p pt 6 ps 2.5 lw 4 dt 4 lc rgb "#1111FF" title "T=25h", "Output/mean_sacc_time/zz_sacc_v_inhib_BOTH_T50.txt" u 1:($3*0.299163/8.01232) w p pt 7 ps 2.5 lw 4 dt 5 lc rgb "#0000DD" title "T=50h", "Output/mean_sacc_time/zz_sacc_v_inhib_BOTH_T100.txt" u 1:($3*0.299163/8.01232) w p pt 8 ps 2.5 lw 4 dt 6 lc rgb "#000099" title "T=100h" 
