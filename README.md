# Code & Data for the paper titled "Stochastic simulation model rationalises the saccharification time courses of various maize lines"

IMPORTANT: Please read the NOTICE and LICENSE files in this repository for important copyright and licensing information regarding the contents of this project.

The folders in this directory contain  used in the publication

The folder "Fitted_saccharification" contains the code, fitted parameters for the different maize lines and the plotting & averaging scripts. The plots are made using gnuplot.

The other appropriately named folders contain the data & the plotting scripts used in the respective cases.
